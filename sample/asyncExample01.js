function aFunc(callback){
    setTimeout(function(){
        console.log('a');
        callback();
    },2000)
}

function bFunc(callback){
    setTimeout(function(){
        console.log('b');
        callback();
    },1000)
}

function cFunc(){
    setTimeout(function(){
        console.log('c');
    },500)
}

aFunc(bFunc(cFunc()));